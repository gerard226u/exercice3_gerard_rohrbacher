import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;

public class Test {

	@org.junit.Test
	public void test() {
			
	}
	
	
	@org.junit.Test
	public void testFormation(){
		
		Formation forma1 = new Formation("form1");
		Identite ident1 = new Identite("gerard226u", "Samuel", "GERARD");	
		Etudiant etu1 = new Etudiant(ident1, forma1);
		
		// test ajout matiere
		Matiere mat = new Matiere("Maths", 4);
		forma1.ajouterMatiere(mat);
		assertEquals("Maths doit �tre ajout�", forma1.getMat().contains(mat), true);
		
		// test suppression matiere
		forma1.supprimerMatiere(mat);
		assertEquals("Maths doit �tre supprim�", forma1.getMat().contains(mat), false);
		
		// test coef
		assertEquals("Le coefficient doit etre egale a 4", forma1.getCoef(mat), 4);
		
	}
	
	
	@org.junit.Test
	public void testEtudiant(){
		
		Formation forma1 = new Formation("form1");
		Identite ident1 = new Identite("gerard226u", "Samuel", "GERARD");	
		Etudiant etu1 = new Etudiant(ident1, forma1);
		Matiere mat = new Matiere("Maths", 4);
		
		// ajouter note
		etu1.ajouterNote(mat, 15);
		HashMap<Matiere, ArrayList<Double>> res = etu1.getResultats();
		assertEquals("La note doit etre ajout�e", 15, 15);
				
	}

	
	@org.junit.Test
	public void testGroupe(){
		Formation forma1 = new Formation("form1");
		Identite ident1 = new Identite("gerard226u", "Samuel", "GERARD");	
		Etudiant etu1 = new Etudiant(ident1, forma1);
		Matiere mat = new Matiere("Maths", 4);
		Groupe grp1 = new Groupe(forma1);
		
		// test ajout etudiant
		grp1.ajouterEtudiant(etu1);
		ArrayList<Etudiant> groupe = grp1.getGroupe();
		boolean res= groupe.contains(etu1);
		assertEquals("L'�tudiant doit �tre ajout� au groupe", res,true);
		
		// test ajout etudiant
		grp1.supprimerEtudiant(etu1);
		ArrayList<Etudiant> groupe2 = grp1.getGroupe();
		boolean res2 = groupe.contains(etu1);
		assertEquals("L'�tudiant doit �tre supprim� du groupe", res2,false);
				
		
	}
	
	
	
}
	
	

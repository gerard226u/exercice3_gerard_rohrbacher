

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeSet;

public class Etudiant{
	
	private Identite id;
	private Formation formation;
	private HashMap <Matiere, ArrayList<Double>> resultat; 
	
	public Etudiant(Identite i, Formation f){
		this.formation = f;
		this.id = i;
	}
	
	
	public void ajouterNote(Matiere mat, double note){
		// on v�rifie que la note soit bien entre 0 et 20
		if(note >= 0 && note <=20){
			// on v�rifie si la matiere existe
			if(resultat.containsKey(mat)){
				resultat.get(mat).add(note);
			}	
			
			// si la matiere n'�xiste pas on la creer 
			else{
				ArrayList<Double> note1;
				note1 = new ArrayList<Double>();
				note1.add(note);
				resultat.put(mat, note1);
			}
		}
	}
	
	
	public int calculerMoyenne(String mat){
		int moyenne = 0;
		// parcour des matieres
		for(int i =0; i<resultat.size();i++){
			// si on trouve la matiere donnee
			if(resultat.containsKey(mat)){
				// parcour des notes de la matiere
				for(int j = 0; i<resultat.get(i).size();j++){
					moyenne += resultat.get(i).get(j);
				}
			}
		}
		return moyenne;
	}
	
	
	public int calculerMoyenneGeneral(){
		int moyenne = 0;
		// parcour de toutes les matieres
		for(int i =0; i<resultat.size();i++){
			// parcours de chaques notes pour chaques matieres
			for(int j = 0; i<resultat.get(i).size();j++){
				moyenne += resultat.get(i).get(j);
			}		
		}
		return moyenne;
	}
	
	public HashMap<Matiere, ArrayList<Double>> getResultats(){
		return this.resultat;
	}
	
	public Formation getFormation(){
		return this.formation;
	}

	public Identite getId(){
		return this.id;
	}
	

}

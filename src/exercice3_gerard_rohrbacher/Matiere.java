
public class Matiere {
	
	private int coef;
	private String nom;
	
	public Matiere(String nom, int coef){
		this.nom = nom;
		this.coef = coef;
	}
	
	public int CoefMat(){
		return this.coef;
	
	}
	
	public String getNom(){
		return this.nom;
	}

	
	
}

import java.util.ArrayList;

public class Groupe {

	private ArrayList<Etudiant> groupe;
	private Formation formation;
	
	
	public Groupe(Formation f){
		groupe = new ArrayList<Etudiant>();
		this.formation = f;
	}
	
	public void ajouterEtudiant(Etudiant etu){
		if(etu.getFormation().equals(this.formation)){
			groupe.add(etu);
		}				
	}
	
	public void supprimerEtudiant(Etudiant etu){
		groupe.remove(etu);
	}
	
	public int calculerMoyenneMat(String mat){
		int moyenne =0;
		for(int i =0;i<groupe.size();i++){
			moyenne += groupe.get(i).calculerMoyenne(mat);
		}
		return moyenne;
	}
	
	
	public int calculerMoyenneGenerale(){
		int moyenne =0;
		for(int i =0; i<groupe.size();i++){
			moyenne += groupe.get(i).calculerMoyenneGeneral();
		}
		return moyenne;
	}
	
	public ArrayList<Etudiant> getGroupe(){
		return this.groupe;
	}
	
}

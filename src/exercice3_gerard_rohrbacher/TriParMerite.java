import java.util.ArrayList;

public class TriParMerite implements Comparable<TriParMerite>{

	private Etudiant etu1;
	private Etudiant etu2;
	
	
	public TriParMerite(Etudiant e1, Etudiant e2){
		this.etu1 = e1;
		this.etu2 = e2;
	}
	
	@Override
	public int compareTo(TriParMerite tpm) {
		int res = 0;
		// tri les etudiants du groupe par leur moyenne generale
		if(etu1.calculerMoyenneGeneral()>etu2.calculerMoyenneGeneral()){
			res = 1;
		}
		if(etu1.calculerMoyenneGeneral()<etu2.calculerMoyenneGeneral()){
			res = -1;
		}
		return res;
	}

}

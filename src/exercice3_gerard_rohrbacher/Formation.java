
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Set;

public class Formation {

	private String identifiant;
	private ArrayList<Matiere> matieres;
	
	public Formation(String id){
		this.identifiant = id;
		matieres = new ArrayList<Matiere>();
	}
	
	public void ajouterMatiere(Matiere mat){
		matieres.add(mat);
	}
	
	public void supprimerMatiere(Matiere mat){
		matieres.remove(mat);
	}
	
	public ArrayList<Matiere> getMat(){
		return this.matieres;
	}
	
	public int getCoef(Matiere mat){	
		return mat.CoefMat();
	}
	
	
	
	
}

public class TriAlpha implements Comparable<TriAlpha>{

	private Etudiant etu1;
	private Etudiant etu2;
	
	
	public TriAlpha(Etudiant e1, Etudiant e2){
		this.etu1 = e1;
		this.etu2 = e2;
	}

	@Override
	public int compareTo(TriAlpha o) {
		// tri les etudiants du groupe par leur ordre alphabetique
		return(etu1.getId().getNom().compareTo(etu2.getId().getNom()));
	}

}
